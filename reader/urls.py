from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from django.contrib.staticfiles import views

urlpatterns = [
    # Admin Panel
    url(r'^admin/', admin.site.urls),
    # django-wysiwyg-redactor
    url(r'^redactor/', include('redactor.urls')),
    # Auth users pages
    url(r'', include('users.urls')),
    #
    url(r'^blog/', include('blog.urls')),
    # Main app, books reader
    url(r'', include('readerBook.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
 