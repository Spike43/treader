from django.contrib import admin
from .models import Activate, UserSettings

# Register your models here.
admin.site.register(Activate)
admin.site.register(UserSettings)
