# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User


class UserSettings(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    user_font_size = models.IntegerField(default=20)
    user_bg_color = models.CharField(default='fff', max_length=6)    
    user_words_on_page = models.IntegerField(default=200)


# User activate model
class Activate(models.Model):
    user_id = models.IntegerField()
    user_key = models.CharField(max_length=36)
		