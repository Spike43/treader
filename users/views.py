# -*- coding: utf-8 -*-
from django.views.generic.edit import FormView
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import AuthenticationForm

from users import forms
from django.views.generic.base import View

from django.contrib import auth
from django.contrib.auth import login
from django.contrib.auth import logout
from django.contrib.auth.models import User

from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect

from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm

from django.core.mail import send_mail
import uuid

from .models import Activate, UserSettings


def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return render(request, 'registration/password_user_reset.html')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'registration/r_settings.html', {
        'form': form
    })


class RegisterFormView(FormView):
    form_class = forms.MyRegistrationForm

    success_url = "/login/"

    template_name = "registration/register.html"

    def form_valid(self, form):
        form.save()
        user = auth.authenticate(
            username=form.cleaned_data['username'], password=form.cleaned_data['password1'])
        auth.login(self.request, user)
        send_activate_mail(form.cleaned_data['email'], self.request.user.id)
        return super(RegisterFormView, self).form_valid(form)


def send_activate_mail(u_email, u_id):
    key = uuid.uuid4()
    send_mail(
        'Subject here',
        'Activate link: SITE_URL/confirm/{0}'.format(key),
        'from@example.com',
        [u_email],
        fail_silently=False,
    )
    Activate.objects.create(user_id=u_id, user_key=key)


def email_confirm(request, token):
    try:
        user_id = Activate.objects.get(user_key=token).user_id
        Activate.objects.get(user_id=user_id, user_key=token).delete()
        return render(request, 'registration/email_confirm_done.html')
    except Activate.DoesNotExist:
        return render(request, 'registration/email_confirm_done.html')


class LoginFormView(FormView):
    form_class = AuthenticationForm

    # Аналогично регистрации, только используем шаблон аутентификации.
    template_name = "registration/login.html"

    # В случае успеха перенаправим на главную.
    success_url = "/"

    def form_valid(self, form):
        # Получаем объект пользователя на основе введённых в форму данных.
        self.user = form.get_user()

        # Выполняем аутентификацию пользователя.
        login(self.request, self.user)
        return super(LoginFormView, self).form_valid(form)


class LogoutView(View):

    def get(self, request):
        # Выполняем выход для пользователя, запросившего данное представление.
        logout(request)

        # После чего, перенаправляем пользователя на главную страницу.
        return HttpResponseRedirect("/")
