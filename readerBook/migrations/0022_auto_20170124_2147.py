# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-24 21:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('readerBook', '0021_auto_20170124_2143'),
    ]

    operations = [
        migrations.RenameField(
            model_name='categorie',
            old_name='title',
            new_name='ru_title',
        ),
        migrations.AddField(
            model_name='categorie',
            name='ua_title',
            field=models.CharField(default=' ', max_length=100),
            preserve_default=False,
        ),
    ]
