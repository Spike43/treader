# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-27 09:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('readerBook', '0011_auto_20161227_1149'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='key',
            field=models.CharField(max_length=5, verbose_name='0'),
        ),
        migrations.AlterField(
            model_name='book',
            name='price',
            field=models.CharField(max_length=70, verbose_name='0'),
        ),
        migrations.AlterField(
            model_name='book',
            name='rates',
            field=models.CharField(max_length=150, verbose_name='0'),
        ),
        migrations.AlterField(
            model_name='book',
            name='rating',
            field=models.CharField(max_length=150, verbose_name='0'),
        ),
        migrations.AlterField(
            model_name='book',
            name='st_rating',
            field=models.CharField(max_length=150, verbose_name='0'),
        ),
    ]
