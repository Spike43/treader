# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-18 12:19
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('readerBook', '0035_auto_20170218_1418'),
    ]

    operations = [
        migrations.DeleteModel(
            name='SiteSetting',
        ),
    ]
