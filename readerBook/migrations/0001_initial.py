# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-10 21:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(
                    auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('b_title', models.CharField(max_length=200)),
                ('b_author', models.CharField(max_length=200)),
                ('b_img', models.FileField(upload_to='uploads/%Y-%m-%d/')),
                ('b_description', models.TextField()),
                ('b_text', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Words',
            fields=[
                ('id', models.AutoField(
                    auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('en_word', models.TextField()),
                ('ru_word', models.TextField()),
            ],
        ),
    ]
