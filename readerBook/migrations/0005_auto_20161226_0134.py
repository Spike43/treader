# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-25 23:34
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('readerBook', '0004_word_en_word_transcription'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='book',
            name='author',
        ),
        migrations.RemoveField(
            model_name='book',
            name='description',
        ),
        migrations.RemoveField(
            model_name='book',
            name='img',
        ),
    ]
