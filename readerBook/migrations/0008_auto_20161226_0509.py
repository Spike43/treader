# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-26 03:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('readerBook', '0007_auto_20161226_0227'),
    ]

    operations = [
        migrations.CreateModel(
            name='Custom',
            fields=[
                ('id', models.AutoField(
                    auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user_id', models.TextField()),
                ('private_book', models.TextField()),
                ('like_pages', models.TextField()),
                ('bookmarks', models.TextField()),
            ],
        ),
        migrations.RenameField(
            model_name='book',
            old_name='fullText',
            new_name='full_text',
        ),
        migrations.AddField(
            model_name='book',
            name='author',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='book',
            name='date',
            field=models.CharField(default='', max_length=50),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='book',
            name='desc',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='book',
            name='img',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='book',
            name='pages',
            field=models.CharField(default='', max_length=20),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='book',
            name='price',
            field=models.CharField(default='', max_length=70),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='book',
            name='rates',
            field=models.CharField(default='', max_length=15),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='book',
            name='rating',
            field=models.CharField(default='', max_length=15),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='book',
            name='seo_keywords',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
