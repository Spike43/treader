# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-24 20:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('readerBook', '0018_auto_20170124_0113'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='book',
            name='rates',
        ),
        migrations.RemoveField(
            model_name='book',
            name='rating',
        ),
        migrations.RemoveField(
            model_name='book',
            name='st_rating',
        ),
        migrations.AddField(
            model_name='book',
            name='readers',
            field=models.CharField(
                default='0', max_length=10, verbose_name='Читателей'),
        ),
        migrations.AlterField(
            model_name='book',
            name='genre',
            field=models.CharField(choices=[(
                '0', 'Приключения'), ('1', 'Хорроры'), ('2', 'Романы')], max_length=5, verbose_name='Жанр'),
        ),
    ]
