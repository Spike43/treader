# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-24 01:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('readerBook', '0017_auto_20170124_0111'),
    ]

    operations = [
        migrations.AlterField(
            model_name='categorie',
            name='category',
            field=models.CharField(max_length=100),
        ),
    ]
