# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-27 10:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('readerBook', '0012_auto_20161227_1153'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserBookmarks',
            fields=[
                ('id', models.AutoField(
                    auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user_id', models.TextField()),
                ('bookmarks', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='UserPrivateBook',
            fields=[
                ('id', models.AutoField(
                    auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user_id', models.TextField()),
                ('private_book', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='UserReadingBook',
            fields=[
                ('id', models.AutoField(
                    auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user_id', models.TextField()),
                ('reading_book', models.TextField()),
            ],
        ),
        migrations.DeleteModel(
            name='CustomUser',
        ),
    ]
