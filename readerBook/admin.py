# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.models import User

from .models import Book
from .models import Word
from .models import Categorie
from .models import UserReadingBook
from .models import UserBookmark
from .models import UserLikedBook
from .models import BookLevel


# Registered models
model_s = { Word, Categorie, BookLevel}

class BookAdmin(admin.ModelAdmin):
	list_display = ('title', 'author', 'readers')
	fieldsets = [
        ('Основная информация', {'fields': ['title', 'author', 'genre', 'lvl']}),
        ('Статистика', {'fields': ['pages', 'readers']}),
        ('Картинка', {'fields': ['img']}),
        ('Hаполнение', {'fields': ['desc', 'full_text']}),
        ('SEO', {'fields': ['seo_desc', 'seo_keywords']}),
    ]



admin.site.register(Book, BookAdmin)

admin.site.register(model_s)

