#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from . import settings

from django.shortcuts import render
from django.utils import timezone

from .models import Book
from .models import Word
from .models import Categorie
from .models import UserBookmark
from .models import UserReadingBook
from .models import UserLikedBook
from .models import BookLevel

from django.http import HttpResponse

from django.shortcuts import render
from django.shortcuts import get_object_or_404

from .scripts import make_word
import json
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template import Context
from django.template import RequestContext


# Get books list
def book_list(request, pk=''):
    books = []
    # Get book of DB "new to old"
    if pk == '':
        books = Book.objects.all()[::-1]
    elif pk == 'liked':

        liked_b = UserLikedBook.objects.filter(user=request.user)

        for book in liked_b:
            books += Book.objects.filter(id=book.liked_book.id)
        if not books:
            error = 'Список избранных книг пуст'
            return render(request, 'reader/error.html', {'error': error})
    else:
        books = Book.objects.filter(genre__category=pk)[::-1]
        if not books:
            books = Book.objects.filter(lvl__lvl=pk)[::-1]
        if not books:
            error = 'Список книг пуст'
            return render(request, 'reader/error.html', {'error': error})

    paginator = Paginator(books, settings.BOOKS_ON_LIST)

    # Get page number
    page = request.GET.get('page')

    # Check input data
    if not isinstance(page, int):
        if isinstance(page, str) and page.isdigit():
            # if page == int number -> navigate into this page
            page = int(page)
            books = paginator.page(page)
        else:
            # if page != int -> open first page
            books = paginator.page(1)

    return render(request, 'reader/books.html', {'books': books})


def book_search(request):
    if request.method == "GET":
        q = request.GET['q']
    else:
        return
    books = []

    books += Book.objects.filter(author__icontains=q)
    books += Book.objects.filter(title__icontains=q)

    paginator = Paginator(books, settings.BOOKS_ON_LIST)

    # Get page number
    page = request.GET.get('page')

    # Check input data
    if not isinstance(page, int):
        if isinstance(page, str) and page.isdigit():
            # if page == int number -> navigate into this page
            page = int(page)
            books = paginator.page(page)
        else:
            # if page != int -> open first page
            books = paginator.page(1)

    return render(request, 'reader/books.html', {'books': books})

# Create your views here.
# Description for book "description.html"


def book_description(request, pk):
    # Get book of DB in index "pk"
    book = get_object_or_404(Book, pk=pk)
    # Show this book
    return render(request, 'reader/description.html', {'book': book})


# Pagination pages of book
def book_page(request, pk):

    if request.user.is_authenticated():
        try:
            UserReadingBook.objects.get(
                user=request.user, reading_book=Book.objects.get(id=pk))

        except UserReadingBook.DoesNotExist:
            UserReadingBook.objects.create(
                user=request.user, reading_book=Book.objects.get(id=pk))

            readers = int(Book.objects.get(pk=pk).readers) + 1
            Book.objects.filter(pk=pk).update(readers=readers)

    # Get book
    i_book = get_object_or_404(Book, pk=pk)
    # Get book text
    book = i_book.full_text.split()
    # On 1 page words
    paginator = Paginator(book, settings.COUNT_WORDS)
    # Get page
    #books = ''
    page = request.GET.get('page')
    try:
        books = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        books = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        books = paginator.page(paginator.num_pages)

   # return render(request, 'list.html', {'contacts': contacts})
    return render(request, 'reader/view.html', {'book': i_book, 'pages': books})


# Get word of "view.html" and translate his
def translate_word(request):
    if request.method == "POST":
        en_word = request.POST.get('wv', '')
        # Get clean word
        en_word = make_word.formatWord(en_word)
        try:
            # Check word in BD
            word = Word.objects.get(en_word=en_word)
            # Get ru_word
            word = word.ru_word + word.en_word + \
                '|' + word.en_word_transcription

            return HttpResponse(word, content_type='text/html')
        except Exception as e:
            ru_word = ''
            transcription = ''
            # Translate word and response
            transc_word = make_word.translateWord(en_word).decode("utf-8")
            # Get word
            ru_word = transc_word.split('::')[0]
            transcription = transc_word.split('::')[1]
            # Make pattern
            word = str(ru_word) + str(en_word) + '|' + str(transcription)
            # Return word patter
            return HttpResponse(word, content_type='text/html')


def bookmarks(request):
    if not request.user.is_authenticated:
        return render(request, 'registration/nlogin.html')

    books = UserBookmark.objects.filter(user=request.user).all()

    result = {}
    for i in books:
        book = Book.objects.get(id=i.book_id)
        result[book] = UserBookmark.objects.filter(book=book, user=request.user)
    if not result:
        error = 'Список закладок пуст'
        return render(request, 'reader/error.html', {'error': error})
    return render(request, 'reader/bookmarks.html', {'result': result})


def set_bookmark(request):  
    # http://127.0.0.1:8000/set_bookmark/?user_id=1&book_id=1&page_id
    if request.method == "POST":
        book_id = int(request.POST.get('bk', ''))
        book = Book.objects.get(id=book_id)
        page_id = request.POST.get('pg', '')
        result = ''
        try:
            a = UserBookmark.objects.get(
                user=request.user, book=book, page_id=page_id)
            a.delete()
            result = 'Deleted'
        except Exception as e:
            UserBookmark.objects.create(
                user=request.user, book=book, page_id=page_id)
            result = 'Added'

        return HttpResponse(result, content_type='text/html')


def set_liked(request, pk):  
    # http://127.0.0.1:8000/set_bookmark/?user_id=1&book_id=1&page_id
    if request.method == "POST":
        book = Book.objects.get(id=pk)
        result = ''

        try:
            UserLikedBook.objects.get(
                user=request.user, liked_book=book).delete()
            result = 'del'
        except UserLikedBook.DoesNotExist:
            UserLikedBook.objects.create(user=request.user, liked_book=book)
            result = 'add'

        # print(result)
        return HttpResponse(result, content_type='text/html')