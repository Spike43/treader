from dynamic_preferences.types import StringPreference, Section, LongStringPreference 
from dynamic_preferences.registries import user_preferences_registry, global_preferences_registry

# we create some section objects to link related preferences together

general = Section('general')

# We start with a global preference
@global_preferences_registry.register
class SiteTitle(StringPreference):
    section = general
    name = 'title'
    default = 'My site'


@global_preferences_registry.register
class SiteTitle(LongStringPreference):
    section = general
    name = 'footer1'
    help_text = 'About'
    default = '''
      <div class="three wide column">
        <h4 class="ui inverted header">About</h4>
        <div class="ui inverted link list">
          <a href="#" class="item">Sitemap</a>
          <a href="#" class="item">Contact Us</a>
          <a href="#" class="item">Religious Ceremonies</a>
          <a href="#" class="item">Gazebo Plans</a>
        </div>
      </div>'''


@global_preferences_registry.register
class SiteTitle(LongStringPreference):
    section = general
    name = 'footer2'
    help_text = 'Services'
    default = '''
      <div class="three wide column">
        <h4 class="ui inverted header">Services</h4>
        <div class="ui inverted link list">
          <a href="#" class="item">Banana Pre-Order</a>
          <a href="#" class="item">DNA FAQ</a>
          <a href="#" class="item">How To Access</a>
          <a href="#" class="item">Favorite X-Men</a>
        </div>
      </div>'''


@global_preferences_registry.register
class SiteTitle(LongStringPreference):
    section = general
    name = 'footer3'
    help_text = 'Footer Header'
    default = '''
      <div class="seven wide column">
        <h4 class="ui inverted header">Footer Header</h4>
        <p>Extra space for a call to action inside the footer that could help re-engage users.</p>
      </div>'''
