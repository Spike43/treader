#!/usr/bin/env python

from ..models import Word
import requests
import re
import json  # Custom library


# Add ne words in DB
def addWord(e_word, r_word, transcription):
    Word.objects.create(
        en_word=e_word, ru_word=r_word, en_word_transcription=transcription)


# Formating word for translate
def formatWord(word):
    word = word.lower()
    # Clear word
    word = re.search(r"[a-z'’-]+", word)
    return str(word.group(0))


# Translate new word in YandexTranslate
def translateWord(en_word):
    key = 'dict.1.1.20161213T154949Z.95119e2f846ee183.99a51ad5651ca35005fc7dc0d46bc9040208cdac'
    # POST method for YandexTranslateAPI
    response = requests.get(
        'https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=' + key + '&lang=en-ru&text=' + en_word + '&ui=ru')

    temp = str(response)
    if temp.split('[')[1] != '200]>':
        # Returt YandexTranslateAPI erorrs
        return bytes(response.text, 'utf-8')
    else:
        # Decode json
        res = json.loads(response.text)
        # print(response)
        en_transcription = ''
        try:
            # Check word on translated
            en_transcription = res['def'][0]['ts']
        except Exception as e:
            rt = 'Not found word: ' + '::' + ''
            # Add don`t translated words into DB
            try:
                a = Word.objects.get(en_word=en_word+' FIXME')
                print(a)
            except Exception as e:
                print(e)
                Word.objects.create(
                    en_word=en_word + " FIXME", ru_word="FIXME", en_word_transcription="FIXME")

            return bytes(rt, 'utf-8')

        ru_word = ''

        # Get max 4 translated words
        for i in range(len(res['def'][0]['tr'])):
            ru_word = ru_word + res['def'][0]['tr'][i]['text'] + ';'
            if i == 4:
                break

        # Add new word into DB
        addWord(en_word, ru_word, en_transcription)
        # Return this word
        rt = ru_word + '::' + en_transcription
        return bytes(rt, 'utf-8')
