#!/usr/bin/env python
# -*- coding: utf-8 -*-
def text_replace(s):
    return (s.replace(u'\u2013', u'-')
        .replace(u'\u0060', u"'")
        .replace('<empty-line />', '')
        .replace(u'\u2014', u'-')
        .replace(u'\xa0', u' ')
        .replace(u'\u2026', u'...')
        .replace(u'\xab', u'<<')
        .replace(u'\xbb', u'>>')
        .replace(u'\u201c', u'"')
        .replace(u'\u201d', u'"')
        .replace(u'\u201e', u'"')
        .replace(u'\u2116', u'No')
        .replace(u'\xad', '')
        .replace(u'\u2019', '\'')
        .replace('<title>', '<strong>')
        .replace('</title>', '</strong>')
        .replace('<emphasis>', '<em>')
        .replace('</emphasis>', '</em>')
       )
