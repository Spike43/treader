from django.conf.urls import url
from . import views
from django.conf import settings

urlpatterns = [
    # Main page
    url(r'^$', views.book_list, name='book_list'),
    url(r'^book/g=(?P<pk>[a-z]+)$', views.book_list, name='book_list'),
    url(r'^search/$', views.book_search, name='book_search'),
    # Description books page
    url(r'^book/(?P<pk>[0-9]+)/$',
        views.book_description, name='book_description'),
    # Page for reading book
    url(r'^book/(?P<pk>[0-9]+)$', views.book_page, name='book_page'),
    # POST methot for translate words
    url(r'translate_word/$', views.translate_word, name='translate_word'),
    # POST method for set bookmark
    url(r'bookmark/$', views.set_bookmark, name='set_bookmark'),
    url(r'bookmarks/$', views.bookmarks, name='bookmarks'),
    # POST method for set bookmark
    url(r'book/(?P<pk>[0-9]+)/like$', views.set_liked, name='set_liked'),
]
