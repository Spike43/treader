from django.apps import AppConfig

# Manage app config


class ReaderbookConfig(AppConfig):
    name = 'readerBook'
