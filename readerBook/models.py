# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.contenttypes.models import ContentType
import math
import re
from .settings import COUNT_WORDS
from readerBook.scripts.text_edit import text_replace
from django.contrib.auth.models import User


class Categorie(models.Model):
    ''' Book categories '''
    category = models.CharField(max_length=100)
    ru_title = models.CharField(max_length=100)

    # Return title for title in Django admin
    def __str__(self):
        return str(self.category)


class BookLevel(models.Model):
    ''' Book categories '''
    lvl = models.CharField(max_length=100)
    title = models.CharField(max_length=100)

    # Return title for title in Django admin
    def __str__(self):
        return str(self.title)


class Book(models.Model):
    ''' Book model - is a great part on this project
        'get_default' - default value in fields
        'COUNT_WORDS' - constant in .settings.py
        Book img =  #background-image:
        TASK: add original readers, and the script that will be count
    '''
    get_default = '0'
    title = models.CharField('Название', max_length=200)
    author = models.CharField('Автор', max_length=100)
    genre = models.ManyToManyField(Categorie)
    lvl = models.ManyToManyField(BookLevel)
    img = models.TextField('Обложка')
    desc = models.TextField('Описание')
    full_text = models.TextField('Текст')

    pages = models.IntegerField('Страниц', default=0)
    readers = models.IntegerField('Читателей', default='0')

    seo_desc = models.TextField('Уникальное описание', default=get_default)
    seo_keywords = models.TextField('Ключевые слова', default=get_default)

    # Return title for title in Django admin
    def __str__(self):
        return str(self.title)
    
    # Before save book in db
    def save(self, *args, **kwargs):
        # Formatting all write-spaces
        self.full_text = re.sub(r">(\s+)", '', self.full_text)
         # Replace unused tags in XML
        self.full_text = text_replace(self.full_text).replace('</p', '</p> ')
        self.pages = str(math.ceil(len(self.full_text.split()) / COUNT_WORDS)) 
        # FIXME - remake counter pages in book
        super(Book, self).save(*args, **kwargs)


class Word(models.Model):
    ''' Word model.
    Save word, word transcription and translate to RU.
    In future i want add UA translate
    '''
    en_word = models.TextField()
    en_word_transcription = models.TextField()
    ru_word = models.TextField()

    # Return title for title in django admin
    def __str__(self):
        return str(self.en_word)


class UserReadingBook(models.Model):
    ''' User reading book
    FIXME: Remake 'user_id' to ForeignKey
    '''
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    reading_book = models.ForeignKey(Book, on_delete=models.CASCADE)

    # Return title for title in django admin
    def __str__(self):
        return str(self.reading_book)


class UserLikedBook(models.Model):
    ''' User liked book
    FIXME: Remake 'user_id' to ForeignKey
    FIXME: Remake 'liked_book' to ForeignKey
    '''
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    liked_book = models.ForeignKey(Book, on_delete=models.CASCADE)

    # Return title for title in django admin
    def __str__(self):
        return str(self.user)


class UserBookmark(models.Model):
    ''' User Bookmarks '''
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    page_id = models.IntegerField()

