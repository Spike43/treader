$('span').click(function() {
    var e = this;
    $.ajax({
      url: '/translate_word/',
      type: "POST",
      data: {wv: $(e).attr('data-wv'), csrfmiddlewaretoken: csrftoken}, // или $(e).data('wv'), или $(e).data().wv
      success: function(word) {
        var ru = word.split(';');
        var en_tr = ru[ru.length-1].split('|');
        var en = en_tr[0];
        var tr = en_tr[1];

        var ru_all = '';
        for(var i = 0; i < ru.length-1; i++){
          ru_all += '<span>' + (i+1) + '. ' + ru[i] + '</span><br>';
        }
        $("span.ru_wrd").html(ru_all);
        $("span.en_wrd").text(en + ' | ');
        $("span.tr_wrd").text('[' + tr + ']');
      }
    });
  });

  $(document).on('click', function(event){
    if($(event.target).hasClass('word') && event.target.tagName.toLowerCase() == 'span')
    {
      var e = event.target;
      var ej = $(event.target);
      var position = ej.position();

      position.top = position.top + 25;
      var translatedBlockCSS = {
        "display": "block",
        "left": position.left + "px",
        "top": position.top + "px"
      };
      $('#translatedBlock').css(translatedBlockCSS);
    }
  });

  $(document).mouseup(function (e) {
    var container = $("#translatedBlock");
    if (container.has(e.target).length === 0){
      container.hide();
    }
  });
