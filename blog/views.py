from django.shortcuts import render, get_object_or_404
from .models import Entry
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.
def entrys(request):
	entrys_list = Entry.objects.all()

	paginator = Paginator(entrys_list, 10) # Show 10 entrys per page

	page = request.GET.get('page')
    
	try:
		entrys = paginator.page(page)
	except PageNotAnInteger:
        # If page is not an integer, deliver first page.
		entrys = paginator.page(1)
	except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
		entrys = paginator.page(paginator.num_pages)

	return render(request, 'blog/entrys.html', {'entrys': entrys})


def post_detail(request, pk):
    entry = get_object_or_404(Entry, pk=pk)
    return render(request, 'blog/entry.html', {'entry': entry})