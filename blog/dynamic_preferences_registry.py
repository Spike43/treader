from dynamic_preferences.types import BooleanPreference, StringPreference, Section
from dynamic_preferences.registries import user_preferences_registry, global_preferences_registry

# we create some section objects to link related preferences together

general = Section('general')
discussion = Section('footer')
blog = Section('blog')


# We start with a global preference
@global_preferences_registry.register
class SiteTitle(StringPreference):
    section = general
    name = 'title'
    default = 'My site'

@global_preferences_registry.register
class SiteDescription(StringPreference):
    section = general
    name = 'description'
    default = 'My desc'



@global_preferences_registry.register
class SiteDescription(StringPreference):
    section = blog
    name = 'title'
    default = 'Clean Blog ;D'

@global_preferences_registry.register
class SiteDescription(StringPreference):
    section = blog
    name = 'desc'
    default = 'A Clean Blog Theme | by Start Bootstrap'
