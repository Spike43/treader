from django.conf.urls import url
from . import views


urlpatterns = [
    # Main page
    url(r'^$', views.entrys, name='entrys'),
	url(r'^post/(?P<pk>[0-9]+)/$', views.post_detail, name='post_detail'),
]