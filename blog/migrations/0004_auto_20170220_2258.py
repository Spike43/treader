# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-20 22:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_entry_text'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='img',
            field=models.ImageField(upload_to=''),
        ),
    ]
