# -*- coding: utf-8 -*-
from django.db import models
from redactor.fields import RedactorField
from django.contrib.auth.models import User


class Entry(models.Model):
    title = models.CharField(max_length=250, verbose_name=u'Название')
    short_text = models.CharField(max_length=250, verbose_name=u'Краткое описание')
    text = RedactorField(verbose_name=u'Полный текст')
    img = models.ImageField()
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateField()