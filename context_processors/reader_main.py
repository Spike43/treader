from readerBook.models import Categorie, UserLikedBook, UserBookmark, BookLevel
from users.models import Activate
from readerBook import settings 


def navigation(request):
    """ Get categories.
    Function show categories links in left-nav on all site pages.
    """
    return {'categories': Categorie.objects.all(), 'levels': BookLevel.objects.all()}


def liked(request):
    """ Check like
    Func check user likes for books
    """
    try:
        book = request.path.split('/')[2]
        if UserLikedBook.objects.filter(user_id=request.user.id, liked_book=book):
            return {'liked': True}
    except:
        pass
    return {'liked': False}


def bookmark(request):
    """ Check bookmarks
    Func check user bookmarks in books
    """
    try:
        book = request.path.split('/')[2]
        page = request.GET['page']
        if UserBookmark.objects.filter(user_id=request.user.id, page_id=page, book_id=book):
            return {'bookmark': True}
    except:
        pass
    return {'bookmark': False}


def is_email(request):
    """ Is_Email
    Check user on activated email
    """
    if Activate.objects.get(user_id=request.user.id):
        return False
    else:
        return True
